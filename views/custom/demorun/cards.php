
  <div class="col-xs-offset-1 col-xs-10 shadow2 padding-20 margin-top-20">
    <h3 class="text-center ">Les 4 étapes du CTE </h3>
    <div class="card col-xs-12 col-md-3">
        <div class="card-body padding-15" style="border: 2px solid <?php echo Yii::app()->session['custom']["color"] ?>; border-radius: 10px;min-height:200px;">
          <h4 class="card-title bold text-dark text-center padding-5" style="border-bottom:1px solid white">
              <i class="margin-5 fa fa-folder-open-o fa-2x"></i><br/>
              Résultat
          </h4> 
          <span class="card-text text-center col-xs-12 no-padding ">
          Lorem ipsum dolor 
          </span>

      </div>
    </div>

    <div class="card col-xs-12 col-md-3">
        <div class="card-body padding-15" style="border: 2px solid <?php echo Yii::app()->session['custom']["color"] ?>; border-radius: 10px;min-height:200px;">
          <h4 class="card-title bold text-dark text-center padding-5" style="border-bottom:1px solid white">
              <i class="margin-5 fa fa-gavel fa-2x"></i><br/>
              Ressources
          </h4> 
          <span class="card-text text-center col-xs-12 no-padding ">
          Lorem ipsum dolor 
          </span>
      </div>
    </div>

    <div class="card col-xs-12 col-md-3">
        <div class="card-body padding-15" style="border: 2px solid <?php echo Yii::app()->session['custom']["color"] ?>; border-radius: 10px;min-height:200px;">
          <h4 class="card-title bold text-dark text-center padding-5" style="border-bottom:1px solid white">
              <i class="margin-5 fa fa-flag-checkered fa-2x"></i><br/>
              Je vote
          </h4> 
          <span class="card-text text-center col-xs-12 no-padding ">
          Lorem ipsum dolor 
          </span>
      </div>
    </div>

    <div class="card col-xs-12 col-md-3">
        <div class="card-body padding-15" style="border: 2px solid <?php echo Yii::app()->session['custom']["color"] ?>; border-radius: 10px;min-height:200px;">
          <h4 class="card-title bold text-dark text-center padding-5" style="border-bottom:1px solid white">
              <i class="margin-5 fa fa-cogs fa-2x"></i><br/>
              J'adhère
          </h4> 
          <span class="card-text text-center col-xs-12 no-padding">
          Lorem ipsum dolor 
          </span>
      </div>
    </div>
  </div>