
<div class="col-xs-12 no-padding" id="customHeader" style="background-color: white">
  <div id="costumBanner" class="col-xs-12 col-sm-12 col-md-9 no-padding">
   <h1>DEMORUN<br/><span class="small">Une nouvelle interface Démo Pratique</span></h1>
  <img class="img-responsive" src='<?php echo Yii::app()->session['custom']["assetsUrl"].Yii::app()->session['custom']["banner"]; ?>'> 
  </div>
  <div class="col-xs-12 col-sm-12 col-md-3 text-center padding-10" >
    <img class="img-responsive logoDescription" src='<?php echo Yii::app()->session['custom']["logo"]?>'> 
    <!--<h2>Une ville Dynamique</h2>-->
    <span style="overflow-y: hidden;max-height: 375px;">
      <span class="col-xs-12 margin-bottom-5">
        <b>Dynamiser le parcours demandeur d'emploi</b> tout en le sécurisant</span><br/>
      <span class="col-xs-12 margin-bottom-5">
        <b>Préparer</b> à l'emploi et <b>réduire</b> la durée
      </span> <br/> 
      <span class="col-xs-12 margin-bottom-5">
        <b>Lever les obstacles à l'emploi</b> : mobilité, garde d'enfant, freins socio économiques
      </span> <br/> 
      <span class="col-xs-12 margin-bottom-5">
        Partir des <b>besoins des entreprises</b> et des <b>compétences attendues</b>
      </span><br/>
      <span class="col-xs-12 margin-bottom-5"><b>
        Développer les compétences</b> transversales et transférables des participants <br/> 
      </span>
      <span class="col-xs-12 margin-bottom-5">
        <b>Mutualiser</b> les moyens
      </span>
    </span>
  </div>
</div>