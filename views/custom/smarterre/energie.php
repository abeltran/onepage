
# TRANSPORT - ÉNERGIE 
Territoire Collaboratif

L’innovation en action au service des habitants du territoire
Une société engagée, mobile et collaborative pour un meilleur respect de l’homme et de la nature

## PRIVILÉGIER LE COVOITURAGE
pour les événements personnels : sorties entre amis, famille
pour les événements culturels : festivals, concerts, etc
pour aller au boulot
La voiture oui, mais pas tout seul !

## LES COMMUNS
Transport en commun 
Télé Travail
Plateforme de coworking, espaces partagés
Décentralisation et délocalisation des institutions et des entreprises

## Faible impact CARbone 
Vélo Électrique
Voiture Électrique
Prendre son vélo ou ses deux jambes pour les petits trajets quotidiens
Repenser les lieux de rencontres et les lieux de travail géographiquement en fonction des participants

## Pratiques / USAGES
Privilégier les voitures électriques et hybrides 
Banir l’auto-soloisme 
Télé Travail
Indemnités kilométriques : salarié qui va au bureau à vélo bénéficie d’un crédit d'impôt
Résilience et Autonomie énergétique

## INNOVATION
Route solaire
Agrinergie 
Innovations 

## Production 
Energies renouvelables 
Solaire et Thermo Solaire 
Eoliennes
Hydraulique
Méthanol végétal
Marée motrice
Géothermie
Modèle distribué : pleins de petites centrales de production

## Moins de transport 
Consommation locale, circuit court 
Plus de Production locale
Moins d’importation 
Autonomie domestique : Maîtrise de l’énergie au niveau de son habitat

## Objectifs
Moins de pollution
Moins d’embouteillages 
Apport social : rencontres, partage, gain de temps, moins de stress 
Économique : Réduction des coûts
Investissement pour une politique globale
STOP au tout voiture !! Repenser l’aménagement des villes et nouveaux quartiers
C’est en faisant des pistes cyclables que l’on a des cyclistes

## Proverbe
L’énergie va là où la pensée va.
Steve Lambert
